#!/bin/bash

LOG_LEVEL=0 # LOG_LEVEL=0 means silent operation
readonly LOG_LEVEL_SILENT="${LOG_LEVEL}"
readonly LOG_LEVEL_FATAL=$((++LOG_LEVEL))
readonly LOG_LEVEL_WARNING=$((++LOG_LEVEL))
readonly LOG_LEVEL_INFO=$((++LOG_LEVEL))
readonly LOG_LEVEL_DEBUG=$((++LOG_LEVEL))
#readonly LOG_LEVEL_SETX=$((++LOG_LEVEL))
readonly LOG_LEVEL_SETX=$((LOG_LEVEL+LOG_LEVEL))
readonly DEFAULT_LOG_LEVEL="${LOG_LEVEL_FATAL}"

LOG_LEVEL="${DEFAULT_LOG_LEVEL}" # by default, print out FATAL messages

function _log() {
    printf '%s: %s\n' "${0}" "${1}" > /dev/stderr
}

function _log_if_requested() {
    if [[ "${LOG_LEVEL}" -ge "${1}" ]] ; then
        _log "${2}"
    fi
}

function _warn() {
    _log_if_requested "${LOG_LEVEL_WARNING}" "[WARN] ${1}"
}

function _info() {
    _log_if_requested "${LOG_LEVEL_INFO}" "[INFO] ${1}"
}

function _debug() {
    _log_if_requested "${LOG_LEVEL_DEBUG}" "[DEBUG] ${1}"
}

function _fail() {
    _log_if_requested "${LOG_LEVEL_FATAL}" "[FATAL] ${1}"
    exit "${2:-1}"
}

function _set_log_level() {
    LOG_LEVEL="${1:-${DEFAULT_LOG_LEVEL}}"
    if [[ "${LOG_LEVEL}" -eq "${LOG_LEVEL_SETX}" ]] ; then
        _debug "'set -x' requested"
        set -x
    fi
    _debug "log level is now: ${LOG_LEVEL}"
}
