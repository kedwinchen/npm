#!/bin/bash

## Solve for the current directory to use as root of the repo
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

# ---------- PREAMBLE ----------

set -eEuo pipefail

# (un-disable) # shellcheck disable=SC1091 # shellcheck not following input because not specified -x
# shellcheck source=./logging.bash
source "${DIR}/logging.bash"

# ---------- VARIABLE DEFINITION SECTION ----------

# ----- Constants -----
__tmp__=0
readonly EXIT_SUCCESS=$((__tmp__++))
readonly EXIT_BAD_ARG=$((__tmp__++))
readonly EXIT_BAD_URI=$((__tmp__++))
readonly EXIT_BAD_SIG=$((__tmp__++))
readonly EXIT_BAD_SUM=$((__tmp__++))
readonly EXIT_ERR_CURL=$((__tmp__++))
readonly EXIT_ERR_DEP=$((__tmp__++))

# TODO: use a better regex; this one should be good enough for now
readonly REGEX_URI='(file|https?):///?([a-z|A-Z|0-9|-]+\.?)+(/.*)*'
# TODO: validate if this is a good decision
readonly TRUSTED_PATH=/bin:/sbin:/usr/bin:/usr/sbin
PATH="${TRUSTED_PATH}"

readonly REQUIRED_COMMANDS=(
    'id'
    'sudo'
    'gpg'
)

# ----- Actual variables -----
TARGET_USER='root'
TARGET_GROUP='root'
TARGET_FILE=''
TRUSTED_KEY_FINGERPRINT=''
TRUSTED_KEY_URI=''

# ---------- FUNCTION DEFINITION SECTION ----------

function print_help() {
    printf 'Usage: %s -f <file URI> [other options]\n' "${0}"
    printf '\t -f (file|http[s]):///absolute/URI/to/file : the URI of the patch to use (currently, expects a shell script) \n'
    printf '\t -u (user|#UID) : the user name or ID to use when executing the patch (defaults to "root") \n'
    printf '\t -g (group|#GID) : the group name or ID to use when executing the patch (defaults to "root") \n'
    printf '\t -k <fingerprint of public key> : the fingerprint of a GPG public key to use to verify the patch \n'
    printf '\t -K (file|http[s]):///absolute/URI/to/public-key : the URI of the GPG public to use to verify the patch (overrides any other specified "-k") \n'
    printf '\t -v : increase verbosity [for logging] by one level (overrides any PREVIOUS -s arguments) \n'
    printf '\t -s : silent mode [for logging] (overrides any PREVIOUS -v arguments) \n'
    printf '\t -h : print this help message and exit \n'
}

function check_opts() {
    if [[ -z "${TARGET_FILE}" ]]
    then
        _fail "length of TARGET_FILE URI appears to be zero (got '${TARGET_FILE}')" "${EXIT_BAD_URI}"
    elif ! [[ "${TARGET_FILE}" =~ ^${REGEX_URI}$ ]]
    then
        # not quoting the right side of '=~', per SC2076
        _fail "TARGET_FILE format does not appear to be correct (wants regex: '${REGEX_URI}') (got: '${TARGET_FILE}')" "${EXIT_BAD_URI}"
    elif [[ -z "${TRUSTED_KEY_FINGERPRINT}" ]] && [[ -z "${TRUSTED_KEY_URI}" ]]
    then
        _fail "Must specify either one of -k or -K" "${EXIT_BAD_ARG}"
    fi
}

function check_commands() {
    for cmd in "${REQUIRED_COMMANDS[@]}"
    do
        _debug "Trying to find: ${cmd}"
        if ! cmd_location="$(PATH="${TRUSTED_PATH}" command -v "${cmd}")"
        then
            _fail "Could not find required command: ${cmd}" "${EXIT_ERR_DEP}"
        else
            _debug "${cmd} is at ${cmd_location}"
        fi
    done
}

function set_permissions() {
    # shellcheck disable=SC2155 # (ignore "declare and use separately")
    # shellcheck disable=SC2034 # (thinks that 'local' is a variable that is not being used)
    {
    readonly local current_uid="$(id -u)"
    readonly local current_gid="$(id -g)"
    readonly local current_user="$(id -u --name)"
    readonly local current_group="$(id -g --name)"
    }

    # per SC2235, using { ... ;} instead of ( ... ) to avoid subshell overhead
    if { [[ "${TARGET_USER}" != "#${current_uid}" ]] && [[ "${TARGET_USER}" != "${current_user}" ]] ;} ||
        { [[ "${TARGET_GROUP}" != "#${current_gid}" ]] && [[ "${TARGET_GROUP}" != "${current_group}" ]] ;}
    then
            _info "requires different privileges"
            _info "currently: user=${current_user},uid=${current_uid},group=${current_group},gid=${current_gid}"
            _info "targeting: user/uid=${TARGET_USER},group/gid=${TARGET_GROUP}"

            # try to prevent sudo from being compromised
            _debug "current sudo is: $(command -v sudo)"
            unalias sudo || true

            _debug "sudo in TRUSTED_PATH is: $(unalias sudo || true ; PATH=${TRUSTED_PATH} command -v sudo)"

            _debug "running the following command as sudo"
            _debug "sudo -u '${TARGET_USER}' -g '${TARGET_GROUP}' -- \\"
            _debug "  '${0}' -u '${TARGET_USER}' -g '${TARGET_GROUP}' -f '${TARGET_FILE}' -k '${TRUSTED_KEY_FINGERPRINT}' -K '${TRUSTED_KEY_URI}'"

            PATH="${TRUSTED_PATH}" \
                sudo -u "${TARGET_USER}" -g "${TARGET_GROUP}" -- \
                "${0}" -u "${TARGET_USER}" -g "${TARGET_GROUP}" -f "${TARGET_FILE}" -k "${TRUSTED_KEY_FINGERPRINT}" -K "${TRUSTED_KEY_URI}"
            exit $?
    fi
}

function get_gpg_key() {
    local possible_key_uri=""
    if [[ -n "${TRUSTED_KEY_FINGERPRINT}" ]]
    then
        possible_key_uri="https://keys.openpgp.org/vks/v1/by-fingerprint/${TRUSTED_KEY_FINGERPRINT}"
        _warn "Only the fingerprint was provided, retrieving the key from the following URI: "
        _warn "'${possible_key_uri}'"
    elif [[ -n "${TRUSTED_KEY_URI}" ]]
    then
        possible_key_uri="${TRUSTED_KEY_URI}"
    fi

    _info "Using the following URI for GPG key:"
    _info "'${possible_key_uri}'"

    _debug "Beginning download and import of GPG key..."
    if possible_pubkey="$(curl -sSL "${possible_key_uri}")"
    then
        _debug "Completed download of the GPG key"
        _debug "Beginning import of the GPG key..."
        if ! { printf '%s' "${possible_pubkey}" | gpg -q --import - &>/dev/null ; }
        then
            _fail "Could not import public key: '${possible_key_uri}'" "${EXIT_BAD_SIG}"
        fi
        _debug "Completed import of the GPG key"
    else
        _fail "Could not download public key from URI: '${possible_key_uri}'" "${EXIT_ERR_CURL}"
    fi
}

function safe_curl_bash() {
    local possible_payload=""
    local possible_signed_checksum=""

    _debug "Beginning download of patch..."
    if ! possible_payload="$(curl -sSL "${TARGET_FILE}")"
    then
        _fail "Error while downloading the patch" "${EXIT_ERR_CURL}"
    fi
    _debug "Completed download of patch"

    # THIS CAN NOT BE A DETACHED SIGNATURE
    # This should be the output of the following command:
    #   gpg --sign -a -u <email> file.sha512sum -o file.sha512sum.sig
    # Where file.sha512sum is the output of the following command:
    #   sha512sum file > file.sha512sum
    _debug "Beginning download of signed checksum..."
    if ! possible_signed_checksum="$(curl -sSL "${TARGET_FILE}.sha512sum.sig")"
    then
        _fail "Error while downloading the signed checksum" "${EXIT_ERR_CURL}"
    fi
    _debug "Completed download of signed checksum"

    # check the signature to be valid (if it is, use it)
    if ! checksum_from_signature="$(printf '%s' "${possible_signed_checksum}" | gpg -q --decrypt - 2>/dev/null)"
    then
        _fail "Could not verify the signature (the signature is probably corrupted)" "${EXIT_BAD_SIG}"
    fi

    # get rid of the filename part of the checksum
    checksum_from_signature="$(printf '%s' "${checksum_from_signature}" | cut -d ' ' -f1 )"

    # using the output of the decryption, validate the checksum of the payload
    # The extra '\n' is necessary since `curl` seems to strip it
    readonly calculated_checksum="$(printf '%s\n' "${possible_payload}" | sha512sum - | cut -d ' ' -f1)"
    if [[ "x${calculated_checksum}" != "x${checksum_from_signature}" ]]
    then
        _info "Calculated checksum: ${calculated_checksum}"
        _info "Decrypted checksum: ${checksum_from_signature}"
        _fail "Calculated payload checksum does not match one from decrypted signature" "${EXIT_BAD_SUM}"
    fi

    # ok, at this point, the checksum *should* be valid, and *should* be trusted
    # this means that it's safe... right? let's execute it!
    # ... what could possibly go wrong?
    printf '%s' "${possible_payload}" | bash
}

function run_main() {
    check_opts
    check_commands
    set_permissions
    get_gpg_key
    safe_curl_bash
}

# ---------- MAIN CODE STARTS HERE ----------

while getopts ':u:g:f:k:K:rvsh' opt
do
    case "${opt}" in
        'f')
            TARGET_FILE="${OPTARG}"
            _info "File is now at URI: '${TARGET_FILE}'"
            ;;
        'u')
            TARGET_USER="${OPTARG}"
            _debug "Target user is now: '${TARGET_USER}'"
            ;;
        'g')
            TARGET_GROUP="${OPTARG}"
            _debug "Target group is now: '${TARGET_GROUP}'"
            ;;
        'k')
            TRUSTED_KEY_FINGERPRINT="$(printf '%s' "${OPTARG}" | tr -d ' ')"
            _info "Using '${TRUSTED_KEY_FINGERPRINT}' for the GPG key fingerprint"
            ;;
        'K')
            TRUSTED_KEY_URI="${OPTARG}"
            _info "Using '${TRUSTED_KEY_URI}' for the GPG key URI"
            ;;
        's')
            _set_log_level "${LOG_LEVEL_SILENT}"
            ;;
        'v')
            _set_log_level "$((++LOG_LEVEL))"
            ;;
        'h')
            print_help
            exit $EXIT_SUCCESS
            ;;
        :)
            print_help
            _fail "argument expected for -${OPTARG}, but none supplied" "${EXIT_BAD_ARG}"
            ;;
        ?)
            print_help
            _fail "invalid/unsupported argument: '${OPTARG}'" "${EXIT_BAD_ARG}"
            ;;
    esac
done

run_main

set +xeEuo pipefail
