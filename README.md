# DISCLAIMER

This software is provided as-is, without any warranty or guarantees, explicit, implied, or otherwise, regardless of what the law requires.

# What

An attempt at writing a "safe" alternative for `curl|bash`.
At least, it tries to be safer (by verifying signatures and checksums)

# TODO

- [ ] Rewrite this in Python/Rust/Go or some other cross-platform language
- [ ] Validate the security
- [ ] Test this more
- [ ] Add more ways to look up public keys (limited by: [https://keys.openpgp.org/about/usage](https://keys.openpgp.org/about/usage))
- [ ] Name the project; possible names:
    - NPM: Not a Package Manager
